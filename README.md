# Docker Phpmyadmin

<p float="left">
  <img src="https://cdn.svgporn.com/logos/docker.svg" height="40" title="docker">
  <img src="https://www.svgrepo.com/show/304556/three-dots.svg" height="30" title="dots">
  <img src="https://upload.wikimedia.org/wikipedia/commons/4/4f/PhpMyAdmin_logo.svg" height="40" title="phpmyadmin">
  <img src="https://www.svgrepo.com/show/304556/three-dots.svg" height="30" title="dots">
  <img src="https://cdn.svgporn.com/logos/mariadb.svg" height="40" title="mariadb">
</p>

Docker + Phpmyadmin + MariaDB

## 📍 Start

- Edit `.env`

- Run:

```bash
task up

# or

docker compose up -d
```

## Taksfile Install (command)

```bash
sh -c "$(curl --location https://taskfile.dev/install.sh)" -- -d
```

## DOCS:

- https://hub.docker.com/_/phpmyadmin
- https://docs.phpmyadmin.net/en/latest/config.html#basic-settings
- https://taskfile.dev/